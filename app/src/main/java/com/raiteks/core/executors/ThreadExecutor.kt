package com.raiteks.core.executors

import java.util.concurrent.BlockingQueue
import java.util.concurrent.Executor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


/**
 * Created by vladimir on 02.06.16.
 */
interface ThreadExecutor : Executor {


    open class DefaultWorker : ThreadExecutor {

        companion object {
            private val INITIAL_POOL_SIZE = Runtime.getRuntime().availableProcessors()
            private val MAX_POOL_SIZE = Integer.MAX_VALUE

            // Sets the amount of time an idle thread waits before terminating
            private val KEEP_ALIVE_TIME = 10

            // Sets the Time Unit to seconds
            private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS
        }

        private val workQueue: BlockingQueue<Runnable>

        private val threadPoolExecutor: ThreadPoolExecutor

        private val threadFactory: ThreadFactory

        init {
            this.workQueue = LinkedBlockingQueue()
            this.threadFactory = JobThreadFactory()
            this.threadPoolExecutor = ThreadPoolExecutor(INITIAL_POOL_SIZE, MAX_POOL_SIZE,
                    KEEP_ALIVE_TIME.toLong(), KEEP_ALIVE_TIME_UNIT, this.workQueue, this.threadFactory)
        }

        override fun execute(runnable: Runnable) {
            this.threadPoolExecutor.execute(runnable)
        }

        private class JobThreadFactory : ThreadFactory {
            companion object {
                private val THREAD_NAME = "android_"
            }

            private var counter = 0

            override fun newThread(runnable: Runnable): Thread =
                    Thread(runnable, THREAD_NAME + counter++)
        }
    }
}
