package com.raiteks.core.ext

import android.view.View

/**
 * Created by vladimir on 25.12.17.
 */
fun View.show() = apply { visibility = View.VISIBLE }
fun View.gone() = apply { visibility = View.GONE }
fun View.invisible() = apply { visibility = View.INVISIBLE }
var View.visible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        if (value) show() else gone()
    }