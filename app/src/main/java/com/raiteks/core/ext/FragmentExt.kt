package com.raiteks.core.ext

import android.support.annotation.IdRes
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.raiteks.R
import com.raiteks.core.android.BaseFragment
import com.trello.navi2.component.support.NaviFragment

fun NaviFragment.replaceIn(
        activity: AppCompatActivity,
        @IdRes containerId: Int = R.id.fragment_container,
        addToBackStack : Boolean = false
) {
    replace(containerId, activity.supportFragmentManager, addToBackStack)
}

fun NaviFragment.replaceIn(
        fragment: BaseFragment<*>,
        @IdRes containerId: Int,
        addToBackStack: Boolean = false
) {
    replace(containerId, fragment.childFragmentManager, addToBackStack)
}

private fun NaviFragment.replace(containerId: Int, fm: FragmentManager, addToBackStack: Boolean = false) {
    val fmt = this
    val tran = fm.beginTransaction()

    if (addToBackStack) tran.addToBackStack(null)

    tran.replace(containerId, fmt)
            .commit()
}
