package com.raiteks.core.ext

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.WindowManager
import io.reactivex.Completable

fun Number.dp(ctx: Context): Int = (ctx.resources.displayMetrics.density * this.toFloat()).toInt()
fun Number.name(ctx: Context): String = ctx.resources.getResourceEntryName(this.toInt())


fun Activity.setResultAndFinish(result: Int, bundle: Bundle) {
    val data = Intent().putExtras(bundle)
    setResult(result, data)
    finish()
}


fun Context.screenSize() : Point {
    val display = (this.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
    val size = Point()
    display.getSize(size)
    return size
}


