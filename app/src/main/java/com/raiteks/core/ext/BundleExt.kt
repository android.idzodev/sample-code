package com.raiteks.core.ext

import android.os.Bundle
import android.os.Parcelable

inline fun <reified T : Parcelable> Bundle?.extract(defaultValueProducer: () -> T): T {
    this ?: return defaultValueProducer()
    val key = T::class.java.name
    if (containsKey(key)) {
        return getParcelable(key)
    } else {
        return defaultValueProducer()
    }
}

fun <T : Parcelable> Bundle.putState(value: T) {
    val name = value::class.java.name
    putParcelable(name, value)
}