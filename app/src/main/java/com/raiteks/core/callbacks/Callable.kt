package com.raiteks.core.callbacks

/**
 * Created by vladimir on 11.09.17.
 */
interface Callable<Data> {
    fun get() : Data
}