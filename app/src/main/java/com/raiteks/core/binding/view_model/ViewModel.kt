package com.raiteks.core.binding.view_model

import android.os.Parcelable
import android.support.annotation.VisibleForTesting
import com.raiteks.core.rx.*
import com.raiteks.core.ext.putState
import com.trello.navi2.Event
import com.trello.navi2.NaviComponent
import com.trello.navi2.rx.RxNavi
import io.mironov.smuggler.AutoParcelable
import io.reactivex.*

interface ViewModel : NaviComponent, CompositeDisposableComponent {

    fun <T : Any> Observable<T>.bindSubscribe(
            onNext: (T) -> Unit = onNextStub,
            onError: (Throwable) -> Unit = onErrorStub,
            onComplete: () -> Unit = onCompleteStub
    ) = subscribe(onNext, onError, onComplete).bind()

    fun <T : Any> Flowable<T>.bindSubscribe(
            onNext: (T) -> Unit = onNextStub,
            onError: (Throwable) -> Unit = onErrorStub,
            onComplete: () -> Unit = onCompleteStub
    ) = subscribe(onNext, onError, onComplete).bind()

    fun <T : Any> Single<T>.bindSubscribe(
            onSuccess: (T) -> Unit = onNextStub,
            onError: (Throwable) -> Unit = onErrorStub
    ) = subscribe(onSuccess, onError).bind()

    fun <T : Any> Maybe<T>.bindSubscribe(
            onSuccess: (T) -> Unit = onNextStub,
            onError: (Throwable) -> Unit = onErrorStub,
            onComplete: () -> Unit = onCompleteStub
    ) = subscribe(onSuccess, onError, onComplete).bind()

    fun Completable.bindSubscribe(
            onComplete: () -> Unit = onCompleteStub,
            onError: (Throwable) -> Unit = onErrorStub
    ) = subscribe(onComplete, onError).bind()
}

class ViewModelImpl(
        naviComponent: NaviComponent,
        val event: Event<*> = Event.DETACH,
        inline val state: Parcelable = object : AutoParcelable {}
) :
        ViewModel,
        NaviComponent by naviComponent,
        CompositeDisposableComponent by CompositeDisposableComponentImpl() {


    init {
        onTerminalEventObserver().bindSubscribe(onNext = { resetCompositeDisposable() })
        saveInstantStateObserver().bindSubscribe(onNext = { it.putState(state) })
    }

    @VisibleForTesting
    fun saveInstantStateObserver() = observe(Event.SAVE_INSTANCE_STATE)

    @VisibleForTesting
    fun onTerminalEventObserver() = observe(event)

    fun <T : Any> NaviComponent.observe(event: Event<T>) = RxNavi.observe(this, event)
}
