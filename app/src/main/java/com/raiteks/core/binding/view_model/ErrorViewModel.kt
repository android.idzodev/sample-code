package com.raiteks.core.binding.view_model

import android.databinding.ObservableField
import com.raiteks.core.android.AppContextWrapper
import android.arch.core.util.Function
import android.content.Context
import android.view.View
import com.raiteks.R

/**
 * Created by vladimir on 19.01.18.
 */

interface ErrorViewModel {
    val errorTitle : ObservableField<String>
    val errorMessage : ObservableField<String>
    val isError : ObservableField<Boolean>
    val isErrorAction : ObservableField<Boolean>

    fun onErrorAction(view: View )
    fun handleError(title: String, message: String, action: Runnable? = null)
    fun hide()
}

class ErrorViewModelImpl(
        override var errorTitle : ObservableField<String> = ObservableField<String>(""),
        override var errorMessage : ObservableField<String> = ObservableField<String>(""),
        override var isError : ObservableField<Boolean> = ObservableField<Boolean>(false),
        override val isErrorAction: ObservableField<Boolean> = ObservableField<Boolean>(false)
        ) : ErrorViewModel {


    override fun onErrorAction(view: View) {
        isError.set(false)
        isErrorAction.set(false)
        sessionAction?.run()
        sessionAction = null
    }

    override fun hide() = isError.set(false)

    private var sessionAction: Runnable? = null

    override fun handleError(title: String, message: String, action: Runnable?) {
        errorTitle.set(title)
        errorMessage.set(message)
        isErrorAction.set(action != null)
        sessionAction = action
        isError.set(true)
    }
}


fun ErrorViewModel.handleNetworkError(context: Context, action: Runnable? = null)
        = this.handleError(
            context.getString(R.string.server_error),
            context.getString(R.string.server_error_message),
            action)
