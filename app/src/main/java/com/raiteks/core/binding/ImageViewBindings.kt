package com.raiteks.core.binding

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.raiteks.core.android.injector

@BindingAdapter(value = ["imageUrl", "imagePlaceholder", "imageCircle","centerInside","imageBluer"], requireAll = false)
fun loadImage(view: ImageView, url: String?, @DrawableRes placeholder: Int?, asCircle: Boolean?, centerInside: Boolean?, isBluered: Boolean) {
    url ?: return
    val imageLoader = view.injector.imageLoader
    if (url != ""){
        imageLoader.load(url).apply {
            when {
                placeholder != null && placeholder > 0 -> {
                    placeholder(placeholder)
                    error(placeholder)
                }
                isBluered == true -> asBluer()
                asCircle == true -> asCircle()
                centerInside == true -> nonCenterCrop()
            }
            into(view)
        }
    } else if (placeholder != null && placeholder != 0){
        imageLoader.load(placeholder).into(view)
    }
}

@BindingAdapter("imageRes")
fun imageRes(view: ImageView, @DrawableRes imageRes: Int) {
    view.setImageResource(imageRes)
}