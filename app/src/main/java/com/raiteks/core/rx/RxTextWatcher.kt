package com.raiteks.core.rx

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.*
import io.reactivex.internal.functions.ObjectHelper
import io.reactivex.plugins.RxJavaPlugins
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import io.reactivex.internal.disposables.DisposableHelper.isDisposed
import android.widget.TextView
import io.reactivex.disposables.Disposable
import android.os.Looper
import java.util.concurrent.atomic.AtomicBoolean


/**
 * Created by vladimir on 28.12.17.
 */

class RxTextWatcher : Observable<String>() {
    var editText: EditText? = null
    private var observer : Observer<in String>? = null

    fun init(editText: EditText){
        this.editText = editText

        if (observer != null) startListen()
    }

    override fun subscribeActual(observer: Observer<in String>?) {
        this.observer = observer
        if (editText != null) startListen()
    }

    private fun startListen(){
        subscribeListener(observer!!)
        observer!!.onNext(getInitialValue());
    }

    private fun subscribeListener(observer: Observer<in String>) {
        val listener = Listener(editText!!, observer)
        observer.onSubscribe(listener)
        editText!!.addTextChangedListener(listener)
    }

    private fun getInitialValue(): String {
        return editText!!.getText().toString()
    }

    internal class Listener(private val view: EditText, private val observer: Observer<in String>) : Disposable, TextWatcher {
        private val unsubscribed = AtomicBoolean()

        override fun isDisposed(): Boolean {
            return unsubscribed.get();
        }

        override fun dispose() {
            if (unsubscribed.compareAndSet(false, true)) {
                view.removeTextChangedListener(this);
            }
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (!isDisposed())
                observer.onNext(s.toString())
        }

        override fun afterTextChanged(s: Editable) {}
    }
}
