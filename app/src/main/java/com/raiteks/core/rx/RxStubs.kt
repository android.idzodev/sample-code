package com.raiteks.core.rx

import android.util.Log
import com.raiteks.domain.exceptions.BaseException
import retrofit2.HttpException

val onNextStub: (Any) -> Unit = {}
val onErrorStub: (Throwable) -> Unit = {
    Log.e("Error", "On Error Stub", it)
    //todo create component that will decide which exception need ingnored
    if (!(it is BaseException) && !(it is HttpException)){
//        todo set crashlitics logs
    }
}
val onCompleteStub: () -> Unit = {}
