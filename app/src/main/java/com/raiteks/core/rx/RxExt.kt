package com.raiteks.core.rx

import com.raiteks.core.executors.ThreadExecutor
import com.raiteks.domain.exceptions.BaseException
import com.raiteks.domain.exceptions.BaseMessageException
import com.raiteks.domain.mappers.ErrorMapper
import com.raiteks.ui.dvo.base.Dvo
import com.stepango.koptional.Optional
import io.reactivex.*
import io.reactivex.Observable.fromIterable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

fun <T : Any> Observable<List<T>>.flatten(): Observable<T> = flatMap { fromIterable(it) }

inline fun <T, U, R> Maybe<T>.zipWith(other: MaybeSource<out U>, crossinline zipper: (T, U) -> R): Maybe<R>
        = zipWith(other, BiFunction { t1, t2 -> zipper(t1, t2) })

fun <T : Collection<Any>> Observable<T>.filterNotEmpty(): Observable<T> = filter { it?.isNotEmpty() ?: false }
fun Observable<String>.filterNotEmptyStr(): Observable<String> = filter { it.isEmpty() }

fun <T> Observable<Dvo<T>>.filterSuccessDvo() = filter { it.isSuccess }.map { it.data!! }
fun <T> Single<Dvo<T>>.filterSuccessDvo() = filter { it.isSuccess }.map { it.data!! }

fun <T : Any> Observable<out Optional<out T>>.filterNonEmpty(): Observable<T> =
        filter { it.isPresent }.map { it.get() }


fun <T : Any> Observable<out Optional<out T>>.nonEmptyOrError(): Observable<T> =
        flatMap { if (it.isPresent) Observable.just(it.get()) else Observable.error(BaseException()) }

fun <T : Any> Observable<T>.subscribeBy(
        onNext: (T) -> Unit = onNextStub,
        onError: (Throwable) -> Unit = onErrorStub,
        onComplete: () -> Unit = onCompleteStub
): Disposable = subscribe(onNext, onError, onComplete)

fun <T : Any> Single<T>.subscribeBy(
        onSuccess: (T) -> Unit = onNextStub,
        onError: (Throwable) -> Unit = onErrorStub
): Disposable = subscribe(onSuccess, onError)

fun Completable.subscribeBy(
        onComplete: () -> Unit = onCompleteStub,
        onError: (Throwable) -> Unit = onErrorStub
): Disposable = subscribe(onComplete, onError)


fun <T : Any> Observable<T>.handleError(errorMapper: ErrorMapper, method: Int = 0): Observable<T> =
        onErrorResumeNext({
            throwable : Throwable -> Observable.error<T>(errorMapper.handleError(throwable, method))
        })

fun <T : Any> Observable<T>.subscribeOn(threadExecutor: ThreadExecutor): Observable<T> =
        subscribeOn(Schedulers.from(threadExecutor))

fun <T : Any> Completable.subscribeOn(threadExecutor: ThreadExecutor): Completable =
        subscribeOn(Schedulers.from(threadExecutor))


fun <T : Any> Single<T>.handleError(errorMapper: ErrorMapper, method: Int = 0): Single<T> =
        onErrorResumeNext({
            throwable : Throwable -> Single.error<T>(errorMapper.handleError(throwable, method))
        })
fun <T : Any> Single<T>.subscribeOn(threadExecutor: ThreadExecutor): Single<T> =
        subscribeOn(Schedulers.from(threadExecutor))










