package com.raiteks.core.images

import android.content.Context
import android.widget.ImageView
import com.raiteks.core.utils.ImageBlurTransformation
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

class PicassoImageLoader : ImageLoader {

    override fun load(url: String?): Request = Request(url = url)
    override fun load(drawableId: Int): Request = Request(drawableId = drawableId)

    class Request @JvmOverloads constructor(
            url: String? = null,
            drawableId: Int = 0
    ) : ImageLoader.Request(url, drawableId) {

        override fun into(imageView: ImageView) {
            makeRequest(imageView.context)
                    ?.networkPolicy(NetworkPolicy.OFFLINE)
                    ?.into(imageView, object : Callback{
                        override fun onSuccess() {}
                        override fun onError() {
                            makeRequest(imageView.context)
                                    ?.into(imageView)
                        }
                    })
        }

        private fun makeRequest(context: Context) = Picasso.with(context).let {
            when {
                url != null && url != "" -> it.load(url)
                drawableId > 0 -> it.load(drawableId)
                else           -> null
            }?.let {
                it.noFade()
                it.fit()
                when {
                    placeholderId > 0 -> it.placeholder(placeholderId)
                    errorId > 0 -> it.error(errorId)
                    isCenterCrop -> it.centerCrop()
                    isBluered -> it.transform(ImageBlurTransformation())
                    else -> it
                }
            }
        }
    }

}