package com.raiteks.core.di.modules



import android.arch.persistence.room.Room
import com.raiteks.R
import com.raiteks.core.android.App
import com.raiteks.domain.db.AppDatabase
import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Created by vladimir on 29.05.16.
 */
@Module
class DataModule {

    @Provides
    @Singleton
    fun provideSession(app: App): AppDatabase = Room.databaseBuilder(app, AppDatabase::class.java, app.getString(R.string.db_name)).build()

}
