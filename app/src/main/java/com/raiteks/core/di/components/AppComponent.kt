package com.raiteks.core.di.components

import com.raiteks.core.di.modules.*
import com.raiteks.core.images.ImageLoader
import com.raiteks.domain.repository.AuthRepository
import com.raiteks.domain.repository.DistanceUnitRepository
import com.raiteks.domain.repository.DrawerRepository
import com.raiteks.domain.repository.PreferenceRepository
import com.raiteks.domain.use_cases.AuthUseCase
import com.raiteks.domain.use_cases.BusinessUseCase
import com.raiteks.domain.use_cases.CategoriesUseCase
import com.raiteks.domain.use_cases.PlacesUseCase
import com.raiteks.managers.LanguageManager
import com.raiteks.managers.PlacesManager
import com.raiteks.managers.search.SearchManager

import javax.inject.Singleton

import dagger.Component

/**
 * Created by vladimir on 25.07.16.
 */
@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        DataModule::class,
        ThreadExecutorsModule::class,
        ApiModule::class,
        UseCaseModule::class,
        RepositoriesModule::class,
        MappersModule::class
))
interface AppComponent {
    val authUseCase : AuthUseCase
    val placesUseCase : PlacesUseCase
    val categoriesUseCase : CategoriesUseCase
    val businessUseCase : BusinessUseCase

    val authRepository : AuthRepository
    val drawerRepository : DrawerRepository
    val distanceUnitRepository : DistanceUnitRepository

    val preferenceRepository : PreferenceRepository
    val searchManager: SearchManager
    val languageManager: LanguageManager
    val placesManager: PlacesManager
    val imageLoader : ImageLoader

    operator fun plus(module: ServicesComponent.ServicesModule): ServicesComponent
}
