package com.raiteks.core.di.scope


import javax.inject.Scope

/**
 * Created by vladimir on 31.05.16.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment
