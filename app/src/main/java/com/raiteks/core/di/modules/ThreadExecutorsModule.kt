package com.raiteks.core.di.modules


import com.raiteks.core.executors.PostExecutionThread
import com.raiteks.core.executors.ThreadExecutor

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Created by vladimir on 02.06.16.
 */
@Module
class ThreadExecutorsModule {

    @Provides
    @Singleton
    @Named("db_executor")
    fun provideDBThreadExecuter(): ThreadExecutor = ThreadExecutor.DefaultWorker()


    @Provides
    @Singleton
    @Named("api_executor")
    fun provideApiThreadExecuter(): ThreadExecutor = ThreadExecutor.DefaultWorker()

}
