package com.raiteks.core.di.modules


import com.raiteks.core.android.App
import com.raiteks.core.android.AppContextWrapper
import com.raiteks.domain.db.AppDatabase
import com.raiteks.domain.repository.*
import javax.inject.Singleton
import dagger.Module
import dagger.Provides


/**
 * Created by vladimir on 02.06.16.
 */
@Module(includes = arrayOf(
        DataModule::class,
        MappersModule::class
))
class RepositoriesModule {


    @Provides
    @Singleton
    fun provideCategoriesRepository(database: AppDatabase) : CategoriesRepository =
            CategoriesRepositoryImpl(database.categoryDao())


}
