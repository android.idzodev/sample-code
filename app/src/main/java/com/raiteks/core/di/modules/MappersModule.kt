package com.raiteks.core.di.modules


import com.google.gson.Gson
import com.raiteks.core.android.App
import com.raiteks.core.android.AppContextWrapper
import com.raiteks.domain.mappers.BusinessMapper
import com.raiteks.domain.mappers.CategoriesMapper
import com.raiteks.domain.mappers.ErrorMapper
import com.raiteks.domain.mappers.PlacesMapper
import com.raiteks.managers.LanguageManager

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Created by vladimir on 09.06.16.
 */
@Module
class MappersModule {
    @Provides
    @Singleton
    fun provideErrorMapper(gson: Gson, app: AppContextWrapper): ErrorMapper = ErrorMapper(gson, app)

    @Provides
    @Singleton
    fun providePlaceMapper(app: AppContextWrapper): PlacesMapper = PlacesMapper(app)

    @Provides
    @Singleton
    fun provideCategoriesMapper(app: AppContextWrapper): CategoriesMapper = CategoriesMapper(app)

    @Provides
    @Singleton
    fun provideBusinessMapper(app: AppContextWrapper): BusinessMapper = BusinessMapper(app)
}
