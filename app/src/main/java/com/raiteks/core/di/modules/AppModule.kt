package com.raiteks.core.di.modules



import com.raiteks.core.android.App
import com.raiteks.core.android.AppContextWrapper
import com.raiteks.core.images.ImageLoader
import com.raiteks.core.images.PicassoImageLoader
import com.raiteks.core.providers.NotificationProvider
import com.raiteks.domain.repository.DistanceUnitRepository
import com.raiteks.managers.search.SearchManager
import com.raiteks.managers.LanguageManager
import com.raiteks.managers.PlacesManager
import com.raiteks.managers.search.BusinessFiltersFactory
import com.raiteks.managers.search.FiltersFactory

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Created by vladimir on 29.05.16.
 */
@Module
class AppModule(private val app: App) {

    @Singleton
    @Provides
    fun provideAppContext(): App = app

    @Singleton
    @Provides
    fun provideImageLoader(): ImageLoader = PicassoImageLoader()

    @Singleton
    @Provides
    fun provideAppContextWrapper(): AppContextWrapper = AppContextWrapper(app)

    @Singleton
    @Provides
    fun provideLanguageManager(appContextWrapper: AppContextWrapper): LanguageManager = LanguageManager(appContextWrapper)

}


