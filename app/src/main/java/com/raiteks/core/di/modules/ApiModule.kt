package com.raiteks.core.di.modules

import android.content.res.Resources

import com.google.gson.Gson
import com.raiteks.BuildConfig
import com.raiteks.core.android.App
import com.raiteks.core.android.AuthRetrofit
import com.raiteks.domain.repository.PreferenceRepository

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

import javax.inject.Named
import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by vladimir on 03.06.16.
 */
@Module
class ApiModule(private val httpUrl: HttpUrl) {

    @Provides
    @Singleton
    @Named("SimpleOkHttp")
    fun provideSimpleOkHttpClient(app: App): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if( BuildConfig.DEBUG ) HttpLoggingInterceptor.Level.BODY
                            else HttpLoggingInterceptor.Level.NONE

        return OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .followRedirects(true)
                .followSslRedirects(true)
                .addInterceptor(interceptor)
                .addNetworkInterceptor { chain ->
                    var request = chain.request()
                    request = request.newBuilder()
                            /*.addHeader("Authorization", "Basic "+ Base64Utils.encodeStringInBase64(
                                    app.getString(R.string.client) + ":"+app.getString(R.string.client_pass), Base64.NO_WRAP
                            ))*/
                            .build()

                    chain.proceed(request)
                }
                .build()
    }

    @Provides
    @Singleton
    @Named("AuthOkHttp")
    fun provideAuthOkHttpClient(preferenceRepository: PreferenceRepository): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()

        interceptor.level = if( BuildConfig.DEBUG ) HttpLoggingInterceptor.Level.BODY
        else HttpLoggingInterceptor.Level.NONE

        return OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .followRedirects(false)
                .followSslRedirects(false)
                .addInterceptor(interceptor)
                .addNetworkInterceptor { chain ->
                    var request = chain.request()
//                    val token = preferenceRepository.getToken();
//                    if (StringUtils.isNullEmpty(token)){
//                        throw IOException("401 Not auth");
//                    }

                    request = request.newBuilder()
//                            .addHeader("Authorization", "Bearer " + token)
                            .build()

                    chain.proceed(request)
                }
                .build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }


    @Singleton
    @Provides
    fun provideSimpleRetrofit(@Named("SimpleOkHttp") okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(httpUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

    }

    @Singleton
    @Provides
    fun provideAuthRetrofit(@Named("AuthOkHttp") okHttpClient: OkHttpClient, gson: Gson): AuthRetrofit {
        return AuthRetrofit(Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(httpUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build())

    }

    companion object {

        fun setNewBaseUrl(retrofit: Retrofit, url: String) {
            val pickerFields = Retrofit::class.java.declaredFields
            for (pf in pickerFields) {
                if (pf.name == "baseUrl") {
                    pf.isAccessible = true
                    try {
                        pf.set(retrofit, HttpUrl.parse(url))
                    } catch (e: IllegalArgumentException) {
                        e.printStackTrace()
                    } catch (e: Resources.NotFoundException) {
                        e.printStackTrace()
                    } catch (e: IllegalAccessException) {
                        e.printStackTrace()
                    }

                }

                if (pf.name == "serviceMethodCache") {
                    pf.isAccessible = true
                    try {
                        (pf.get(retrofit) as ConcurrentHashMap<*, *>).clear()
                    } catch (e: IllegalArgumentException) {
                        e.printStackTrace()
                    } catch (e: Resources.NotFoundException) {
                        e.printStackTrace()
                    } catch (e: IllegalAccessException) {
                        e.printStackTrace()
                    }

                }
            }
        }
    }
}
