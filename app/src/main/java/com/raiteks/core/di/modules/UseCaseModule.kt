package com.raiteks.core.di.modules

import com.raiteks.core.executors.PostExecutionThread
import com.raiteks.core.executors.ThreadExecutor
import com.raiteks.domain.db.AppDatabase
import com.raiteks.domain.mappers.BusinessMapper
import com.raiteks.domain.mappers.CategoriesMapper
import com.raiteks.domain.mappers.ErrorMapper
import com.raiteks.domain.mappers.PlacesMapper
import com.raiteks.domain.repository.*
import com.raiteks.domain.use_cases.*
import com.raiteks.managers.LanguageManager
import com.raiteks.managers.search.FiltersFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by vladimir on 02.06.16.
 */
@Module(includes = arrayOf(
        RepositoriesModule::class,
        ThreadExecutorsModule::class,
        ApiModule::class
))
class UseCaseModule {

    @Provides
    @Singleton
    fun providePlaceUseCase(
            @Named("db_executor")
            dbExecutor: ThreadExecutor,
            @Named("api_executor")
            apiExecutor: ThreadExecutor,
            uiExecutor: PostExecutionThread,
            placesRepository: RecentPlaceRepository,
            currentPlaceRepository: CurrentPlaceRepository,
            placesMapper: PlacesMapper
    ): PlacesUseCase =
            PlacesUseCaseImpl(dbExecutor, placesRepository, currentPlaceRepository)

    @Provides
    @Singleton
    fun provideCategoriesUseCase(
            @Named("db_executor")
            dbExecutor: ThreadExecutor,
            @Named("api_executor")
            apiExecutor: ThreadExecutor,
            uiExecutor: PostExecutionThread,
            preferenceRepository: PreferenceRepository,
            errorMapper: ErrorMapper,
            categoriesMapper: CategoriesMapper,
            languageManager: LanguageManager,
            categoriesRepository: CategoriesRepository,
            cuisinesRepository: CuisinesRepository,
            currentPlaceRepository: CurrentPlaceRepository,
            appDatabase: AppDatabase,
            retrofit: Retrofit
    ): CategoriesUseCase =
            CategoriesUseCaseImpl(
                    dbExecutor,
                    apiExecutor,
                    retrofit,
                    errorMapper,
                    categoriesMapper,
                    categoriesRepository,
                    cuisinesRepository,
                    currentPlaceRepository,
                    appDatabase,
                    languageManager
            )


    @Provides
    @Singleton
    fun provideBusinessUseCase(
            @Named("db_executor")
            dbExecutor: ThreadExecutor,
            @Named("api_executor")
            apiExecutor: ThreadExecutor,
            uiExecutor: PostExecutionThread,
            preferenceRepository: PreferenceRepository,
            errorMapper: ErrorMapper,
            businessMapper: BusinessMapper,
            languageManager: LanguageManager,
            retrofit: Retrofit,
            filtersFactory: FiltersFactory
    ): BusinessUseCase =
            BusinessUseCaseImpl(filtersFactory, apiExecutor, dbExecutor,  retrofit ,errorMapper, businessMapper, languageManager)


    @Provides
    @Singleton
    fun provideAuthUseCase(
            @Named("db_executor")
            dbExecutor: ThreadExecutor,
            @Named("api_executor")
            apiExecutor: ThreadExecutor,
            uiExecutor: PostExecutionThread,
            retrofit: Retrofit,
            errorMapper: ErrorMapper,
            preferenceRepository: PreferenceRepository,
            authRepository: AuthRepository,
            drawerRepository: DrawerRepository
    ): AuthUseCase =
            AuthUseCaseImpl(retrofit, errorMapper, apiExecutor, authRepository, drawerRepository)
}
