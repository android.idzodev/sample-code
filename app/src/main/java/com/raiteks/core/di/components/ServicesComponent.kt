package com.raiteks.core.di.components

import com.raiteks.core.di.scope.PerService

import dagger.Module
import dagger.Subcomponent

/**
 * Created by vladimir on 27.07.16.
 */
@Subcomponent(modules = arrayOf(
        ServicesComponent.ServicesModule::class
))
@PerService
interface ServicesComponent {


    @Module
    class ServicesModule {

    }
}
