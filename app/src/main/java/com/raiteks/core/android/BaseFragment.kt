package com.raiteks.core.android

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.raiteks.core.utils.ViewUtils
import com.raiteks.ui.screens.MainActivity
import com.trello.navi2.component.support.NaviFragment

/**
 * Created by vladimir on 28.10.17.
 */
abstract class BaseFragment<T : ViewDataBinding> : NaviFragment() {

    abstract val layoutId: Int

    lateinit var binding: T

    abstract fun initBinding(binding: T, state: Bundle?)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        // Workaround for http://stackoverflow.com/questions/27057449/when-switch-fragment-with-swiperefreshlayout-during-refreshing-fragment-freezes
        return if (binding.root is SwipeRefreshLayout)
            FrameLayout(getActivity()).apply { addView(binding.root) }
        else binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBinding(binding, savedInstanceState)
    }

    val activity by lazy { (getActivity() as AppCompatActivity) }

    /**
     * Method that quick show message to user
     * @param title title of message
     * @param message body of message
     */
    fun showInfoPopup(title: String, message: String) {
        Toast.makeText(context, title, Toast.LENGTH_SHORT).show()
    }

    /**
     * Convenient method to setup toolbar if it's inside fragment
     *
     * @param toolbar fragment toolbar
     * @param text string resource address of toolbar title text
     * @param icon toolbar navigation icon
     * @param onNavigationClickListener listener of toolbar navigation button
     */
    protected fun setupToolbar(toolbar: Toolbar, @StringRes text: Int, @DrawableRes icon: Int?, onNavigationClickListener: () -> Unit) {
        setupToolbar(toolbar, context?.getString(text), icon, onNavigationClickListener)
    }

    /**
     * Convenient method to setup toolbar if it's inside fragment
     *
     * @param toolbar fragment toolbar
     * @param text toolbar title text
     * @param icon toolbar navigation icon
     * @param onNavigationClickListener listener of toolbar navigation button
     */
    protected fun setupToolbar(
            toolbar: Toolbar,
            text: String?,
            @DrawableRes icon: Int?,
            onNavigationClickListener: () -> Unit
    ) {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowHomeEnabled(true)
        if (text != null)
            toolbar.title = text
        if (icon != null)
            toolbar.setNavigationIcon(icon)

        toolbar.setNavigationOnClickListener(View.OnClickListener { onNavigationClickListener() })
    }

    /**
     * Convenient method to handle [AppCompatActivity.onBackPressed] called from activity
     */
    fun onBackPressed() : Boolean {
        if (activity?.supportFragmentManager?.backStackEntryCount!! > 0){
            activity?.supportFragmentManager?.popBackStack()
            return true
        }

        activity?.onBackPressed()
        return true
    }
}