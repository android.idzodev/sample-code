package com.raiteks.core.android

import android.content.Context

/**
 * Created by vladimir on 22.12.17.
 */

class AppContextWrapper(context: Context){

    var context: Context = context
        private set

    fun init(context: Context){
        this.context = context
    }

    fun getString(res: Int) : String = context.getString(res)
    fun getString(res: Int, vararg args : Any) : String = context.getString(res, args)
}
