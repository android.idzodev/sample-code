package com.raiteks.core.android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.raiteks.R;

/**
 * Created by vladimir on 24.02.17.
 */

public abstract class BasePopup extends BaseActivity {

    protected FrameLayout vContainer;
    protected TextView vPositive;
    protected TextView vNegative;

    public abstract View createView(LayoutInflater inflater, ViewGroup parent, Bundle saveInstanceState);

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_container);
        vContainer = findViewById(R.id.view_container);
        vPositive = findViewById(R.id.positive);
        vNegative = findViewById(R.id.negative);
        vNegative.setOnClickListener(v -> onNegativeClicked());
        vPositive.setOnClickListener(v -> onPositiveClicked());

        vContainer.addView(createView(LayoutInflater.from(this), vContainer, savedInstanceState));
    }

    public void onPositiveClicked(){
        setResult(RESULT_OK);
        finish();
    }

    public void onNegativeClicked(){
        setResult(RESULT_CANCELED);
        finish();
    }

    protected void showPosivite(boolean show, String text){
        vPositive.setText(text);
        vPositive.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    protected void showNegative(boolean show, String text){
        vNegative.setText(text);
        vNegative.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        onNegativeClicked();
    }
}
