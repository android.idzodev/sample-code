package com.raiteks.core.android

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

/**
 * Created 26.01.2016.
 */
abstract class BasePreference(val context: Context) {

    protected val preference = PreferenceManager.getDefaultSharedPreferences(context)

    protected fun save(key: String, value: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.edit().putString(key, value).commit()
    }

    protected fun save(key: String, value: Long) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.edit().putLong(key, value).commit()
    }

    protected fun save(key: String, value: Boolean) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.edit().putBoolean(key, value).commit()
    }

    protected fun save(key: String, value: Int) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.edit().putInt(key, value).commit()
    }

    protected fun save(key: String, value: Float) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.edit().putFloat(key, value).commit()
    }

    protected fun remove(key: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.edit()
                .remove(key)
                .commit()
    }

    protected fun getStr(key: String, def: String = ""): String {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(key, def)
    }

    protected fun getInt(key: String, def: Int = 0): Int {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getInt(key, def)
    }

    protected fun getBoolean(key: String, def: Boolean): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getBoolean(key, def)
    }

    protected fun getLong(key: String, def: Long = 0): Long {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getLong(key, def)
    }

    protected fun getFloat(key: String, def: Float = 0f): Float {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getFloat(key, def)
    }
}
