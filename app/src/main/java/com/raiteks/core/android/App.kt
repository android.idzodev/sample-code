package com.raiteks.core.android

import android.app.Application
import android.content.Context
import com.raiteks.R
import com.raiteks.core.di.components.AppComponent
import com.raiteks.core.di.components.DaggerAppComponent
import com.raiteks.core.di.modules.*
import com.jakewharton.picasso.OkHttp3Downloader
import com.raiteks.managers.LanguageManager
import com.squareup.leakcanary.LeakCanary
import com.squareup.picasso.Picasso
import net.danlew.android.joda.JodaTimeAndroid
import okhttp3.HttpUrl
import okhttp3.OkHttpClient

/**
 * Created by vladimir on 28.10.17.
 */
class App : Application() {

    companion object {
        operator fun invoke(): AppComponent = injector

        lateinit var appComponent : AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this)
        JodaTimeAndroid.init(this)

        setupAppComponent()
        setupPicasso()
    }

    private fun setupAppComponent() {
        val url = HttpUrl.parse(getString(R.string.baseUrl))

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .apiModule(ApiModule(url))
                .threadExecutorsModule(ThreadExecutorsModule())
                .dataModule(DataModule())
                .mappersModule(MappersModule())
                .build()
    }

    private fun setupPicasso() {
        val picasso = Picasso.Builder(this)
                .downloader(OkHttp3Downloader(OkHttpClient()))
                .build()

        Picasso.setSingletonInstance(picasso)
    }
}

val Any.injector: AppComponent by lazy { App.appComponent }
inline fun <T> lazyInject(crossinline block: AppComponent.() -> T): Lazy<T> = lazy { App.appComponent.block() }
