package com.raiteks.core.android

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.raiteks.R
import com.raiteks.managers.LanguageManager
import com.raiteks.ui.events.EmptyEvent
import com.stepango.koptional.toOptional
import com.trello.navi2.component.support.NaviAppCompatActivity


/**
 * Created by vladimir on 28.10.17.
 */
abstract class BaseActivity : NaviAppCompatActivity() {

    private val t = ""

    open val layoutId = R.layout.fragment_activity
    open val fragmentContainerId = R.id.fragment_container

    abstract fun initStartFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        setContentView()
        showStartFragment(savedInstanceState)
    }

    protected open fun setContentView(){
        setContentView(layoutId)
    }

    protected fun showStartFragment(savedInstanceState: Bundle?){
        if (supportFragmentManager.findFragmentById(R.id.fragment_container) == null) {
            if (savedInstanceState != null) return
            initStartFragment()
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(injector.languageManager.onAttach(newBase!!))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val fragment = supportFragmentManager.findFragmentById(fragmentContainerId).toOptional()
        if (fragment.isPresent) fragment.get().onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else super.onBackPressed()

    }

    open fun onToolbarClicked() {}
}

tailrec fun Context.asBaseActivity(): BaseActivity? {
    if (this is BaseActivity) {
        return this
    } else if (this is ContextWrapper) {
        return this.baseContext.asBaseActivity()
    }
    return null
}