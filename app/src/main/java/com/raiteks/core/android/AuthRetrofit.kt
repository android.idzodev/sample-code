package com.raiteks.core.android

import retrofit2.Retrofit

/**
 * Created by vladimir on 08.06.16.
 */
class AuthRetrofit(private val retrofit: Retrofit) {

    fun <T> create(api: Class<T>): T = retrofit.create(api)
}
