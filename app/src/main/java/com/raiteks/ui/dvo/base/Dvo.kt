package com.raiteks.ui.dvo.base

/**
 * Created by vladimir on 20.01.18.
 */

open class Dvo<Data> (
    val data: Data?,
    val error: Throwable?
) {
    val isSuccess
        get() = error == null
}

fun <T : Any> T?.toDvo() = Dvo<T>(this, null)
fun <T : Any> Throwable.toDvo() = Dvo<T>(null, this)