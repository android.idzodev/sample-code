package com.raiteks.ui.dvo.base

import com.raiteks.domain.exceptions.BaseMessageException

/**
 * Created by vladimir on 20.01.18.
 */

class PageDvo<Data>(
        val page: Int,
        data: Data?,
        error: BaseMessageException?
) : Dvo<Data>(data, error)
