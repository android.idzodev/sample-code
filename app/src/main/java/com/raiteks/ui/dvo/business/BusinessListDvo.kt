package com.raiteks.ui.dvo.business

/**
 * Created by vladimir on 21.12.17.
 */

class BusinessListDvo(
        val business : List<AbsBusinessDvo>,
        val page: Int = 1,
        val hasMorePages : Boolean = false
)
