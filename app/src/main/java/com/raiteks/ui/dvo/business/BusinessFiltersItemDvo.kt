package com.raiteks.ui.dvo.business

/**
 * Created by vladimir on 24.01.18.
 */

class BusinessFiltersItemDvo(
        val filtersName : String,
        val filtersCount : String
) : AbsBusinessDvo() {
    override val stableId: Long = "filters_${System.currentTimeMillis()}_".hashCode().toLong()


    fun isFilters() = filtersName.isNotEmpty()
}
