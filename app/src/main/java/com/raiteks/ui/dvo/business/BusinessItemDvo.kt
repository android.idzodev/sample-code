package com.raiteks.ui.dvo.business

import com.raiteks.domain.api.dto.BusinessItemDto


/**
 * Created by vladimir on 21.12.17.
 */

data class BusinessItemDvo(
        val id : Long,
        val image : String,
        val title : String,
        val address : String,
        val cuisines : String,
        val rating : Float,
        val ratingCount : String,
        val advTitle : String?
) : AbsBusinessDvo() {
    override val stableId = id

    constructor(item: BusinessItemDto) : this(
            item.id,
            item.image ?: "",
            item.name,
            item.address ?: "",
            item.cuisines ?: "",
            item.rating,
            "(${item.rating_count})",
            null)
}
