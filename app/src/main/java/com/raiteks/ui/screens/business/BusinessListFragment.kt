package com.raiteks.ui.screens.business

import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.RadioGroup
import com.github.nitrico.lastadapter.LastAdapter
import com.raiteks.R
import com.raiteks.core.android.BaseFragment
import com.raiteks.core.android.lazyInject
import com.raiteks.core.binding.view_model.*
import com.raiteks.core.ext.swap
import com.raiteks.core.ext.with
import com.raiteks.core.rx.filterNonEmpty
import com.raiteks.core.rx.filterSuccessDvo
import com.raiteks.databinding.BusinessListFragmentBinding
import com.raiteks.domain.entities.actions.GetBusinessAction
import com.raiteks.domain.exceptions.NetworkConnectionException
import com.raiteks.managers.search.FilterDvo
import com.raiteks.managers.search.SearchManager
import com.raiteks.ui.dvo.base.Dvo
import com.raiteks.ui.dvo.business.AbsBusinessDvo
import com.raiteks.ui.dvo.business.BusinessFiltersItemDvo
import com.raiteks.ui.dvo.business.BusinessItemDvo
import com.raiteks.ui.dvo.widgets.RadioButtonDvo
import com.stepango.rxdatabindings.setTo
import com.trello.navi2.NaviComponent

/**
 * Created by vladimir on 04.01.18.
 */

class BusinessListFragment : BaseFragment<BusinessListFragmentBinding>(){

    companion object {
        val TAG = BusinessListFragment::class.java.name
        private const val ARGS_TITLE = "ARGS_TITLE"

        fun newInstance(title: String) = BusinessListFragment().apply {
            arguments = Bundle().apply {
                putString(ARGS_TITLE, title)
            }
        }
    }

    override val layoutId = R.layout.business_list_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun initBinding(binding: BusinessListFragmentBinding, state: Bundle?) {
        binding.vm = BusinessListViewModel(
                activity,
                binding.drawer,
                this,
                arguments?.getString(ARGS_TITLE) ?: "")
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.menu_business, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when(item!!.itemId){
                R.id.menu_business_filter -> { binding.vm?.openDrawer();true}
                R.id.menu_business_map -> { ; true }
                else -> super.onOptionsItemSelected(item)
            }
}

class BusinessListViewModel(
        val activity: AppCompatActivity,
        drawerLayout: DrawerLayout,
        naviComponent: NaviComponent,
        toolbarTitle : String
) : ViewModel by ViewModelImpl(naviComponent),
    LoaderViewModel by LoaderViewModelImpl() {

    val filters = ObservableField<List<FilterDvo>>(listOf())
    val items = ObservableField<List<AbsBusinessDvo>>(listOf())

    val filersViewModel = FiltersViewModelnew(filters, { display() }, naviComponent, drawerLayout)
    val errorViewModel = ErrorViewModelImpl()
    val toolbarViewModel = ToolbarViewModelImpl(R.drawable.ic_arrow_white, toolbarTitle, { activity.onBackPressed() })

    val businessUseCase by lazyInject { businessUseCase }
    val placesUseCase by lazyInject { placesUseCase }

    init {
        businessUseCase.getBusinessFilters({ activity })
                .doOnSubscribe {
                    showLoader()
                    errorViewModel.hide()
                }
                .setTo(filters)
                .doOnSuccess { display() }
                .bindSubscribe()
    }

    private fun display()
            = placesUseCase.currentPlace()
            .flatMapSingle { businessUseCase.getBusiness(GetBusinessAction(it.latitude, it.longitude, filters.get()))  }
            .doOnSubscribe {
                showLoader()
                errorViewModel.hide()
            }
            .doOnNext {
                hideLoader()
                handleError(it)
            }
            .filterSuccessDvo()
            .doOnNext {
                if (it.business.isEmpty()){
                    errorViewModel.handleError(
                            activity.getString(R.string.no_result),
                            activity.getString(R.string.messsage_change_filter_criteria),
                            Runnable { openDrawer() })
                }
            }
            .setTo(items, { it.business })
            .bindSubscribe()

    private fun <T> handleError(it : Dvo<T>){
        if (!it.isSuccess) when(it.error){
            is NetworkConnectionException -> errorViewModel.handleNetworkError(activity, Runnable { display() })
        }
    }

    fun openDrawer(){
        filersViewModel.openDrawer()
    }

    fun closeDrawer(){
        filersViewModel.closeDrawer()
    }
}

@BindingAdapter("businessAdapter")
fun businessAdapter(v: RecyclerView, list: List<AbsBusinessDvo>) {
    LastAdapter.with(list)
            .map(BusinessItemDvo::class.java, R.layout.business_item2)
            .map(BusinessFiltersItemDvo::class.java, R.layout.business_filter_label_item)
            .swap(v)
}



