package com.raiteks.ui.screens.business

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.raiteks.R
import com.raiteks.core.android.App
import com.raiteks.core.android.BaseActivity
import com.raiteks.core.android.BaseFragment
import com.raiteks.core.android.injector
import com.raiteks.core.ext.replaceIn

/**
 * Created by vladimir on 14.12.17.
 */

class BusinessActivity : BaseActivity() {

    companion object {
        const val MODE_SEARCH = 1
        const val MODE_BUSINESS = 2

        private const val EXTRA_MODE = "EXTRA_MODE"
        private const val EXTRA_TITLE = "EXTRA_TITLE"

        fun startSearch(activity: Activity){
            val intent = Intent(activity, BusinessActivity::class.java)
            intent.putExtra(EXTRA_MODE, MODE_SEARCH)
            intent.putExtra(EXTRA_TITLE, activity.getString(R.string.search))
            activity.startActivity(intent)
        }

        fun startBusinessList(activity: Activity, title : String){
            val intent = Intent(activity, BusinessActivity::class.java)
            intent.putExtra(EXTRA_MODE, MODE_BUSINESS)
            intent.putExtra(EXTRA_TITLE, title)
            activity.startActivity(intent)
        }
    }



    override fun initStartFragment() {
        val searchManager = injector.searchManager
        val mode = intent.getIntExtra(EXTRA_MODE, MODE_SEARCH)

        if (!searchManager.canSearch() || mode == MODE_SEARCH)
            SearchFragment
                    .newInstance()
                    .replaceIn(this, fragmentContainerId)
        else
            BusinessListFragment
                    .newInstance(intent.getStringExtra(EXTRA_TITLE))
                    .replaceIn(this, fragmentContainerId)
    }


    override fun onToolbarClicked() {
        onBackPressed()
    }
}
