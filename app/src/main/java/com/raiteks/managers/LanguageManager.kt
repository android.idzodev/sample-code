package com.raiteks.managers

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import android.util.DisplayMetrics
import com.raiteks.R

import com.raiteks.core.android.App
import com.raiteks.core.utils.StringUtils
import com.raiteks.domain.entities.Lang
import com.raiteks.domain.repository.PreferenceRepository

import java.util.Locale
import com.raiteks.R.string.language
import com.raiteks.R.string.language
import android.preference.PreferenceManager
import android.content.SharedPreferences
import com.raiteks.R.string.language
import com.raiteks.core.android.AppContextWrapper


/**
 * Created by vladimir on 12.12.17.
 */
@SuppressLint("StaticFieldLeak")
class LanguageManager(
        val appContextWrapper: AppContextWrapper
) {
    companion object {
        private const val SELECTED_LANGUAGE = "SELECTED_LANGUAGE"
    }


    val langNames : Array<Lang> = arrayOf(
            Lang("English", "en"),
            Lang("Українська", "ua"),
            Lang("Руский", "ru")
    )
    val activeLanguage: Lang
        get() {
            for (item in langNames)
                if (activeLanguageCode == item.code)
                    return item

            throw RuntimeException("No language with active code: ${ activeLanguageCode }")
        }

    private var langCodes : Array<String> = arrayOf()
    private var activeLanguageCode : String = "en"

    init {
        val context = appContextWrapper.context
        langCodes = Array(langNames.size, { i -> langNames[i].code })
        //init selected lang or default

        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val defLang = getDefaultLangCode(context)
        changeLanguage(context, preferences.getString(SELECTED_LANGUAGE, defLang))
    }

    fun changeLanguage(context: Context, langCode: String) {
        if (!validate(langCode)) return

        saveActivatedLangCode(context, langCode)
        activeLanguageCode = langCode
        appContextWrapper.init(doChangeLanguage(langCode, context))
    }

    private fun doChangeLanguage(langCode: String, context: Context) : Context{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val locale = Locale(langCode)
            Locale.setDefault(locale)

            val configuration = context.resources.configuration
            configuration.setLocale(locale)
            configuration.setLayoutDirection(locale)

            return context.createConfigurationContext(configuration)
        }

        val locale = Locale(langCode)
        Locale.setDefault(locale)

        val resources = context.resources

        val configuration = resources.configuration
        configuration.locale = locale
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale)
        }

        resources.updateConfiguration(configuration, resources.displayMetrics)

        return context
    }

    private fun saveActivatedLangCode(context: Context, langCode: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()

        editor.putString(SELECTED_LANGUAGE, langCode)
        editor.commit()
    }

    private fun getDefaultLangCode(context: Context) : String{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val localList = context.resources.configuration.locales
            for (i in 0..localList.size()){
                if (validate(localList[i].language))
                    return localList[i].language
            }
        }
        else {
            val currentLocale = context.resources.configuration.locale
            if (validate(currentLocale.language))
                return currentLocale.language
        }

        return "en"
    }

    private fun validate(code: String) : Boolean {
        return langCodes.any { it == code }
    }

    fun onAttach(context: Context): Context {
        return doChangeLanguage(activeLanguageCode, context)
    }
}
