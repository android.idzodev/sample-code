package com.raiteks.domain.mappers

import com.raiteks.R
import com.raiteks.core.android.AppContextWrapper
import com.raiteks.domain.api.response.BusinessListResponse
import com.raiteks.managers.search.FilterDvo
import com.raiteks.managers.search.SearchManager
import com.raiteks.ui.dvo.base.Dvo
import com.raiteks.ui.dvo.base.toDvo
import com.raiteks.ui.dvo.business.AbsBusinessDvo
import com.raiteks.ui.dvo.business.BusinessFiltersItemDvo
import com.raiteks.ui.dvo.business.BusinessItemDvo
import com.raiteks.ui.dvo.business.BusinessListDvo
import kotlinx.android.synthetic.main.business_filter_label_item.view.*

/**
 * Created by vladimir on 21.12.17.
 */

class BusinessMapper(
        val app: AppContextWrapper
) {

    fun map(it: BusinessListResponse, selectedFilters : List<FilterDvo>? = null): Dvo<BusinessListDvo> {
        val business = arrayListOf<AbsBusinessDvo>()
        if (selectedFilters != null && selectedFilters.isNotEmpty()) {
            val builder = StringBuilder()

            selectedFilters.forEachIndexed  { index, item ->
                builder.append(item.nameWithSelectedValues)
                if (index != selectedFilters.size-1) builder.append(", ")
            }

            val filtersName = builder.toString()
            val filtersCount = app.getString(R.string.filters_n, selectedFilters.size)
            business.add(BusinessFiltersItemDvo(filtersName, filtersCount))
        }

        business.addAll(it.business.map { BusinessItemDvo(it) })
        return BusinessListDvo(business).toDvo()
    }
}
