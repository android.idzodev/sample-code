package com.raiteks.domain.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.raiteks.domain.api.dto.CategoryDto

/**
 * Created by vladimir on 20.12.17.
 */
@Entity
data class Category(
        @PrimaryKey(autoGenerate = false)
        var id: Long,
        var title: String,
        var image: String,
        var rest_counts: Int
) {
        constructor(categoryDto: CategoryDto) :
                this(categoryDto.id, categoryDto.title, categoryDto.image, categoryDto.rest_counts)
}
