package com.raiteks.domain.repository

import com.raiteks.domain.api.dto.CategoryDto
import com.raiteks.domain.api.dto.CuisinesDto
import com.raiteks.domain.db.dao.CategoryDao
import com.raiteks.domain.entities.Category
import com.raiteks.domain.entities.Cuisine
import com.stepango.koptional.Optional
import com.stepango.koptional.toOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by vladimir on 20.12.17.
 */

interface CategoriesRepository : KeyValueRepo<Long, Category>{
    fun observePreview(): Observable<List<Category>>
}


class CategoriesRepositoryImpl(val categoryDao: CategoryDao) : CategoriesRepository {
    override fun observePreview(): Observable<List<Category>> = categoryDao.getPreviewCategories().toObservable()

    override fun save(value: Category): Single<Category> = Single.create {
        categoryDao.insert(value)
        it.onSuccess(value)
    }

    override fun remove(key: Long): Completable = Completable.fromAction{
        categoryDao.delete(key)
    }

    override fun observe(key: Long): Observable<Optional<Category>> = categoryDao.getCategory(key).toObservable()
            .map { it.toOptional() }

    override fun save(data: List<Category>): Single<List<Category>> = Single.create {
        if (data.isNotEmpty()) categoryDao.insert(data)
        it.onSuccess(data)
    }

    override fun remove(keys: Set<Long>): Completable = Completable.fromAction{
        categoryDao.delete(keys)
    }

    override fun removeAll(): Completable = Completable.fromAction{
        categoryDao.deleteAll()
    }

    override fun observeAll(): Observable<List<Category>> = categoryDao.getCategories().toObservable()

}
