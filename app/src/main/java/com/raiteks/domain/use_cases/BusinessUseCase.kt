package com.raiteks.domain.use_cases

import android.content.Context
import com.raiteks.core.android.injector
import com.raiteks.core.executors.ThreadExecutor
import com.raiteks.core.rx.handleError
import com.raiteks.core.rx.subscribeOn
import com.raiteks.core.utils.DateUtils
import com.raiteks.domain.api.BusinessApi
import com.raiteks.domain.api.request.GetBusinessRequest
import com.raiteks.domain.api.response.BusinessListResponse
import com.raiteks.domain.entities.actions.GetBusinessAction
import com.raiteks.domain.mappers.BusinessMapper
import com.raiteks.domain.mappers.ErrorMapper
import com.raiteks.managers.LanguageManager
import com.raiteks.managers.search.BusinessFiltersFactory
import com.raiteks.managers.search.FilterDvo
import com.raiteks.managers.search.FiltersFactory
import com.raiteks.managers.search.SearchManager
import com.raiteks.ui.dvo.base.Dvo
import com.raiteks.ui.dvo.base.toDvo
import com.raiteks.ui.dvo.business.BusinessListDvo
import io.reactivex.Single
import retrofit2.Retrofit

/**
 * Created by vladimir on 20.12.17.
 */

interface BusinessUseCase {
    fun getBusiness(getBusinessAction: GetBusinessAction, page: Int = 1) : Single<Dvo<BusinessListDvo>>
    fun getBusinessFilters(context: () -> Context) : Single<List<FilterDvo>>
}

class BusinessUseCaseImpl(
        val businessFiltersFactory : FiltersFactory,
        val apiExecutor : ThreadExecutor,
        val dbExecutor: ThreadExecutor,
        retrofit : Retrofit,
        val errorMapper: ErrorMapper,
        val businessMapper: BusinessMapper,
        val languageManager: LanguageManager
) : BusinessUseCase {
    private val businessApi = retrofit.create(BusinessApi::class.java)


    override fun getBusiness(getBusinessAction: GetBusinessAction, page: Int): Single<Dvo<BusinessListDvo>>
            = updateBusiness(getBusinessAction, page)
            .map { businessMapper.map(it, getBusinessAction.filters) }
            .onErrorResumeNext { t: Throwable -> Single.just(t.toDvo())}
            .subscribeOn(apiExecutor)

    override fun getBusinessFilters(context: () -> Context): Single<List<FilterDvo>>
            = businessFiltersFactory.createFilters(context)
            .subscribeOn(dbExecutor)

    private fun updateBusiness(getBusinessAction: GetBusinessAction, page: Int): Single<BusinessListResponse>
            = businessApi.getBusiness(
                languageManager.activeLanguage.code,
                getBusinessAction.latitude,
                getBusinessAction.longitude,
                GetBusinessRequest(getBusinessAction.filters.associateBy ({ it.id },{  it.selectedValues.map { it.id }})),
                page
            )
            .handleError(errorMapper)
}
