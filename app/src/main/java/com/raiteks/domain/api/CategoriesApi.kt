package com.raiteks.domain.api

import com.raiteks.domain.api.response.CategoriesDtoResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by vladimir on 19.12.17.
 */

interface CategoriesApi {

    @GET("v1/categories/{lang}/{lat}/{lng}")
    fun getCategories(
            @Path("lang") lang : String,
            @Path("lat") lat : Double,
            @Path("lng") lng : Double
    ) : Single<CategoriesDtoResponse>

}
