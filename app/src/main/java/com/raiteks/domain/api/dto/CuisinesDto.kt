package com.raiteks.domain.api.dto

/**
 * Created by vladimir on 19.12.17.
 */

data class CuisinesDto(
        val id : Long,
        val title : String,
        val image : String,
        val rest_counts : Int
)
