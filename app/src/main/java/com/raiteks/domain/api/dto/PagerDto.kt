package com.raiteks.domain.api.dto

/**
 * Created by vladimir on 21.12.17.
 */

class PagerDto(
        val page: Int,
        val last_page: Int,
        val has_more : Boolean
)
