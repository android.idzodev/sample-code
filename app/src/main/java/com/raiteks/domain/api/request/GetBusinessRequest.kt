package com.raiteks.domain.api.request

/**
 * Created by vladimir on 26.01.18.
 */

class GetBusinessRequest(
        val filters: Map<String,List<String>>
)
