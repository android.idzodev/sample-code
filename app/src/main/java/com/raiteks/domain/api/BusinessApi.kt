package com.raiteks.domain.api

import com.raiteks.domain.api.request.GetBusinessRequest
import com.raiteks.domain.api.response.BusinessListResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by vladimir on 21.12.17.
 */

interface BusinessApi {
    @POST("v1/business/{lang}/{lat}/{lng}")
    fun getBusiness(
            @Path("lang") lang : String,
            @Path("lat") lat : Double,
            @Path("lng") lng : Double,
            @Body searchRequest: GetBusinessRequest,
            @Query("page") page: Int
    ) : Single<BusinessListResponse>
}
