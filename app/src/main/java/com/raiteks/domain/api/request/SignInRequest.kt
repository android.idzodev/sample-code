package com.raiteks.domain.api.request

/**
 * Created by vladimir on 15.12.17.
 */
data class SignInRequest(
        val username : String,
        val password : String
)