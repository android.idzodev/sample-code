package com.raiteks.domain.api

import com.raiteks.domain.api.dto.TokenDto
import com.raiteks.domain.api.request.RefreshRequest
import com.raiteks.domain.api.request.SignInRequest
import com.raiteks.domain.api.request.SignUpRequest
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by vladimir on 15.12.17.
 */
interface AuthApi {

    @POST("v1/signin")
    fun signIn(
            @Body request: SignInRequest
    ) : Single<TokenDto>


    @POST("v1/signup")
    fun signUp(
            @Body request: SignUpRequest
    ) : Single<TokenDto>


    @POST("v1/refresh")
    fun refresh(
            @Body request: RefreshRequest
    ) : Single<TokenDto>

}