package com.raiteks.domain.api.request

/**
 * Created by vladimir on 15.12.17.
 */
data class RefreshRequest(
        val refresh_token : String
)