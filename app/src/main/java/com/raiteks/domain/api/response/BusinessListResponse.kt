package com.raiteks.domain.api.response

import com.raiteks.domain.api.dto.BusinessItemDto
import com.raiteks.domain.api.dto.PagerDto

/**
 * Created by vladimir on 21.12.17.
 */

class BusinessListResponse(
        val pager: PagerDto,
        val business: Array<BusinessItemDto>
)
