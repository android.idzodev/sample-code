package com.raiteks.domain.api.dto

/**
 * Created by vladimir on 15.12.17.
 */

class TokenDto(
        val access_token : String,
        val refresh_token : String,
        val expires_in : Int,
        val user : ShortUserDto
)
