package com.raiteks.domain.api.response

import com.raiteks.domain.api.dto.CategoryDto
import com.raiteks.domain.api.dto.CuisinesDto
import com.raiteks.domain.api.dto.MenuItemDto

/**
 * Created by vladimir on 19.12.17.
 */

data class CategoriesDtoResponse(
        val categories : Array<CategoryDto>,
        val cuisines : Array<CuisinesDto>,
        val menu : Array<MenuItemDto>
)
