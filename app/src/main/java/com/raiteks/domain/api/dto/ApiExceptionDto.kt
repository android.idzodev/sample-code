package com.raiteks.domain.api.dto

import com.google.gson.annotations.SerializedName
import com.raiteks.R
import com.raiteks.core.android.AppContextWrapper
import com.raiteks.domain.exceptions.BaseMessageException


/**
 * Created by vladimir on 25.01.17.
 */

class ApiExceptionDto{
    @SerializedName("error_code")
    var error_code: Int = 400
    @SerializedName("app_code")
    var app_code: Int = 400
}
