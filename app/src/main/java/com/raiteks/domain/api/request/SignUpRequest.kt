package com.raiteks.domain.api.request

/**
 * Created by vladimir on 15.12.17.
 */
data class SignUpRequest(
        val email : String,
        val fname : String,
        val lname : String,
        val password : String
)