package com.raiteks.domain.api.dto

/**
 * Created by vladimir on 21.12.17.
 */

class BusinessItemDto(
        val id : Long,
        val name : String,
        val cuisines : String?,
        val image : String?,
        val address : String?,
        val rating : Float,
        val rating_count : Int
)
