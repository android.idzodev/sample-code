package com.raiteks.domain.api.dto

/**
 * Created by vladimir on 15.12.17.
 */

class ShortUserDto (
        val id: Long,
        val fname: String,
        val lname: String,
        val image: String?
)
