package com.raiteks.domain.exceptions.api

import com.raiteks.domain.exceptions.BaseMessageException

/**
 * Created by vladimir on 22.01.18.
 */

class EmailAlreadyExistException(title: String, message: String) : BaseMessageException(title, message)
