package com.raiteks.domain.exceptions.api

import com.raiteks.domain.exceptions.BaseMessageException

/**
 * Created by vladimir on 21.01.18.
 */

class InvalidCredException(title: String, message: String) : BaseMessageException(title, message)
