package com.raiteks.domain.exceptions

import android.content.Context

/**
 * Created by vladimir on 21.01.18.
 */

class GoogleApiNotInitException : BaseException()