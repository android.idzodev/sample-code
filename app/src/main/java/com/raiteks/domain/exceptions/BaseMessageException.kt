package com.raiteks.domain.exceptions

import android.support.v7.widget.DialogTitle

/**
 * Created by vladimir on 15.12.17.
 */

open class BaseMessageException(
        val title: String,
        override val message: String
) : BaseException()
