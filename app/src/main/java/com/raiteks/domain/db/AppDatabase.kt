package com.raiteks.domain.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.raiteks.domain.db.dao.CategoryDao
import com.raiteks.domain.db.dao.CuisineDao
import com.raiteks.domain.db.dao.PlaceDao
import com.raiteks.domain.entities.Category
import com.raiteks.domain.entities.Cuisine
import com.raiteks.domain.entities.Place
import com.raiteks.domain.mappers.ErrorMapper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by vladimir on 27.12.17.
 */
@Database(entities = [
    Category::class
],version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun categoryDao() : CategoryDao
}

fun <T : Any> Observable<T>.transaction(database: AppDatabase, block: (T) -> Completable)
        = flatMapCompletable {
            Completable.fromAction {
                database.runInTransaction { block(it).blockingAwait() }
            }
        }

fun <T : Any> AppDatabase.transaction(items : T, block: (T) -> Completable)
        = Observable.just(items)
        .doOnNext {
            runInTransaction { block(it).blockingAwait() }
        }
