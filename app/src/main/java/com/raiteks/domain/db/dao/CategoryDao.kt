
package com.raiteks.domain.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.raiteks.domain.entities.Category
import com.raiteks.domain.entities.Cuisine
import io.reactivex.Flowable
import io.reactivex.Observable

/**
 * Created by vladimir on 28.12.17.
 */
@Dao
interface CategoryDao : BaseDao<Category> {
    @Query("select * from category order by title limit 6")
    fun getPreviewCategories() : Flowable<List<Category>>

    @Query("select * from category order by title")
    fun getCategories() : Flowable<List<Category>>

    @Query("select * from category where id = :id")
    fun getCategory(id : Long) : Flowable<Category>

    @Query("DELETE from category WHERE id = :id")
    fun delete(id : Long)

    @Query("DELETE from category WHERE id IN (:ids)")
    fun delete(ids : Set<Long>)

    @Query("DELETE from category")
    fun deleteAll()
}
